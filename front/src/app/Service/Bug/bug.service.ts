import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bug } from '../models/bug';

@Injectable({
  providedIn: 'root'
})
export class BugService {

  constructor(private http:HttpClient) { }

  getAllBugs(){
    return this.http.get('http://localhost:8081/api/bug')
  }
  getOneBug(id:number){
    // return this.http.get('http://localhost:8081/api/bug/'+id)
    return this.http.get(`http://localhost:8081/api/bug/${id}`)
  }

  addBug(bug:Bug){
    return this.http.post('http://localhost:8081/api/bug',bug)
    
  }
  updateBug(bug:Bug){
    return this.http.put('http://localhost:8081/api/bug',bug)
  }
  deleteBug(id:number){
    
    return this.http.delete(`http://localhost:8081/api/bug/${id}`, { responseType: 'text' });
  }
}
