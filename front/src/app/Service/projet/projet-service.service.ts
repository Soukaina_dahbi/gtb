import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Projet } from '../../models/projet';

@Injectable({
  providedIn: 'root'
})
export class ProjetServiceService {

  constructor(private http:HttpClient) { }
  getAllUser(){
    return this.http.get('http://localhost:8081/api/projet')
  }
  getOneBug(id:number){
    // return this.http.get('http://localhost:8081/api/projet/'+id)
    return this.http.get(`http://localhost:8081/api/projet/${id}`)
  }

  addBug(projet:Projet){
    return this.http.post('http://localhost:8081/api/projet',projet)
    
  }
  ubdateBugs(projet:Projet){
    return this.http.put('http://localhost:8081/api/projet',projet)
  }
  deleteBugs(id:number){
    return this.http.delete(`http://localhost:8081/api/projet/${id}`,{responseType:'text'})
  }
}
