import { TestBed } from '@angular/core/testing';

import { ProjetServiceService } from './projet-service.service';

describe('ProjetServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjetServiceService = TestBed.get(ProjetServiceService);
    expect(service).toBeTruthy();
  });
});
