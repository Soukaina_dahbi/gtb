import { Component, OnInit } from '@angular/core';
import {AjouteModel} from '../models/ajouter.model';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';




@Component({
  selector: 'app-ajouter',
  templateUrl: './ajouter.component.html',
  styleUrls: ['./ajouter.component.css']
})
export class AjouterComponent implements OnInit {
  user:AjouteModel=new AjouteModel();
  ajouterFrom:FormGroup;
  constructor(private formBuilder:FormBuilder) { }

  ngOnInit() {
    
    this.ajouterFrom=this.formBuilder.group({
      'id':[this.user.id,
        Validators.required

      ],
      'nom':[this.user.nom,
       Validators.required,
      ],
      'prenom':[this.user.prenom,
       Validators.required
      ],
      'Email':[this.user.Email,
       Validators.required,
       Validators.email
       
      ],
      'password':[this.user.password,
       Validators.required,
       Validators.minLength(6),
       Validators.maxLength(30)
      ],
      'role':[this.user.role,
       Validators.required
      ]
    });
    
    
  }

}
